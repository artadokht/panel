import React, { useState, useEffect } from "react";
import { Table } from "reactstrap";
// import SubscribersGained from "../../ui-elements/cards/statistics/SubscriberGained"
// import RevenueGenerated from "../../ui-elements/cards/statistics/RevenueGenerated"
// import QuaterlySales from "../../ui-elements/cards/statistics/QuaterlySales"
// import OrdersReceived from "../../ui-elements/cards/statistics/OrdersReceived"
// import RevenueChart from "../../ui-elements/cards/analytics/Revenue"
// import GoalOverview from "../../ui-elements/cards/analytics/GoalOverview"
// import BrowserStats from "../../ui-elements/cards/analytics/BrowserStatistics"
// import ClientRetention from "../../ui-elements/cards/analytics/ClientRetention"
// import SessionByDevice from "../../ui-elements/cards/analytics/SessionByDevice"
// import CustomersChart from "../../ui-elements/cards/analytics/Customers"
// import ChatWidget from "../../../components/@vuexy/chatWidget/ChatWidget"

import "../../assets/scss/plugins/charts/apex-charts.scss";
import DashboardHeader from "./../../components/DashboardHeader/DashboardHeader";
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap";
import {
  getStudents,
  getStudentsForPagination,
} from "../../core/api/studentServices";
import Pagination from "../../components/common/Pagination/Pagination";

const Dashboard = () => {
  const [students, setStudents] = useState([]);
  const [page, setPage] = useState(1);

  // useEffect(async () => {
  //   setStudents(await getStudents());
  // }, []);

  useEffect(async () => {
    setStudents(await getStudentsForPagination(page, 10));
  }, [page]);

  const handleStateChange = (pageNumber) => {
    setPage(pageNumber);
  };

  return (
    <>
      <DashboardHeader />
      <Card>
        <CardHeader>
          <CardTitle className="text-primary">
            <strong>لیست دانشجویان</strong>
          </CardTitle>
        </CardHeader>
        <CardBody className="text-center">
          <Table hover responsive bordered>
            <thead>
              <tr>
                <th>نام و نام خانوادگی</th>
                <th>ایمیل</th>
                <th>تاریخ تولد</th>
                <th>تاریخ ثبت نام</th>
                <th>شماره همراه</th>
              </tr>
            </thead>
            <tbody>
              {students.map((student) => (
                <React.Fragment>
                  <tr key={student._id}>
                    <td>{student.fullName}</td>
                    <td>{student.email}</td>
                    <td>{student.birthDate}</td>
                    <td>{student.registerDate}</td>
                    <td>{student.phoneNumber}</td>
                  </tr>
                </React.Fragment>
              ))}
            </tbody>
          </Table>
          {students && (
            <Pagination
              itemCount={students.length}
              pageSize={5}
              setActive={handleStateChange}
              active={page}
            />
          )}
        </CardBody>
      </Card>
    </>
  );
};

export default Dashboard;
