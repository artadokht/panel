const news = {
  allNews: `${process.env.REACT_APP_Host}/api/news`,
  newsPagination: `${process.env.REACT_APP_Host}/api/news/list`,
  _commentNewsPagination:
    "queryString: ?pagenumber=1&pagesize=4&category='article'",
  topNews: `${process.env.REACT_APP_Host}/api/news/topNews`,
  newsById: `${process.env.REACT_APP_Host}/api/news/`,
  _commentNewsById: "after slash = :newsId",

  category: `${process.env.REACT_APP_Host}/api/news/category/`,
  _commentCategory: "after slash = :category",
  addNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  updateNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentUpdateNewsArticle: "after slash = :newsId",
  deleteNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentDeleteNewsArticle: "after slash = :newsId",
};

const article = {
  topArticle: `${process.env.REACT_APP_Host}/api/topArticles`,
};

const newsArticle = {
  category: `${process.env.REACT_APP_Host}/api/news/category/`,
  _commentCategory: "after slash = :category",
  addNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  updateNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentUpdateNewsArticle: "after slash = :newsId",
  deleteNewsArticle: `${process.env.REACT_APP_Host}/api/news/`,
  _commentDeleteNewsArticle: "after slash = :newsId",
};

const course = {
  getAllCourses: `${process.env.REACT_APP_Host}/api/course`,
  getCourseById: `${process.env.REACT_APP_Host}/api/course/`,
  _commentGetCourseById: "after slash = :courseId",
  coursePagination: `${process.env.REACT_APP_Host}/api/course/list`,
  _commentCoursePagination: "queryString: ?pagenumber=1&pagesize=4",
  getCourseForTerm: `${process.env.REACT_APP_Host}/api/course/term/`,
  _commentGetCourseForTerm: "after slash = :termId",
  addCourse: `${process.env.REACT_APP_Host}/api/course/add`,
  updateCourse: `${process.env.REACT_APP_Host}/api/course/`,
  _commentUpdateCourse: "after slash = :courseId",
  deleteCourse: `${process.env.REACT_APP_Host}/api/course/`,
  _commentDeleteCourse: "after slash = :courseId",
};

const regLog = {
  registerMember: `${process.env.REACT_APP_Host}/api/auth/register`,
  loginMember: `${process.env.REACT_APP_Host}/api/auth/login`,
  registerEmployee: `${process.env.REACT_APP_Host}/api/auth/employee/register`,
  loginEmpployee: `${process.env.REACT_APP_Host}/api/auth/employee/login`,
};

const password = {
  forgetPassword: `${process.env.REACT_APP_Host}/api/forgetpassword`,
  resetPassword: `${process.env.REACT_APP_Host}/api/resetPassword`,
  _commentResetPassword: "after slash :token",
};

const students={
  getAllStudents:`${process.env.REACT_APP_Host}/api/student/getall`,
  getStudentsForPagination:`${process.env.REACT_APP_Host}/api/student/list`,
  getStudentsById:`${process.env.REACT_APP_Host}/api/student/:id`,
  updateStudentInfo:`${process.env.REACT_APP_Host}/api/student/604226ac48b2ba00ec7197fe`,
  deleteStudentById:`${process.env.REACT_APP_Host}/api/student/:id`,
  activeStudent:`${process.env.REACT_APP_Host}/api/student/active/5dcc4010df4e0f0850478206`,
  deactiveStudent:`${process.env.REACT_APP_Host}/api/student/deactive/5dcc4103df4e0f0850478207`,
}
export { news, article, newsArticle, course, regLog, password,students };
