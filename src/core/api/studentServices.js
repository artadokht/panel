import http from "./httpServices";
import {students} from "./apiUrl";

const getStudents=async()=>{
  try{ 
      const {data}=await http.get(students.getAllStudents);
      console.log(data.result);
      return data.result;
    }
  catch(error){ 
      return null;
  }
}

const getStudentsForPagination=async(pageNumber,pageSize)=>{
  try{
    const{data}=await http.get(students.getStudentsForPagination + `?pagenumber=${pageNumber}&pagesize=${pageSize}`);
    return data.result.students;
   }
  catch(error){ 
    return null;
  }
}
export {getStudents,getStudentsForPagination};