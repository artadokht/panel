import axios from "axios";
import { toast } from "react-toastify";
import Cookies from "js-cookie";

const tokenKey = "token";

axios.interceptors.response.use(null, (error) => {

  console.log("error", error.response);
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  // console.log("interceptor " + expectedError);
  if (!expectedError) {
    toast.error("خطایی رخ داده است. لطفا دوباره تلاش کنید.");
  } else {
    console.log("run");
    toast.error(error.response.data.message.message[0].message);
  }

  return Promise.reject(error);
});

setJwt(getJwt());

export function getJwt() {
  return Cookies.get(tokenKey);
  // return localStorage.getItem(tokenKey);
}

 function setJwt(jwt) { 
  axios.defaults.headers.common["x-auth-token"] = jwt;
}

export default {
  get: axios.get,
  put: axios.put,
  post: axios.post,
  delete: axios.delete,
  setJwt,
};
