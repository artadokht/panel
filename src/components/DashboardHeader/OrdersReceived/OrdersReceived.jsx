import React from "react";
import { Users } from "react-feather";
import { ordersReceived, ordersReceivedSeries } from "../StatisticsData";
import StatisticsCards from "./../../@vuexy/statisticsCard/StatisticsCard";

const OrdersReceived = () => {
  return (
    <>
      <StatisticsCards
        icon={<Users className="warning" size={22} />}
        iconBg="warning"
        stat="30 نفر"
        statTitle="تعداد کل اساتید"
        options={ordersReceived}
        series={ordersReceivedSeries}
        type="area"
      />
    </>
  );
};

export { OrdersReceived };
