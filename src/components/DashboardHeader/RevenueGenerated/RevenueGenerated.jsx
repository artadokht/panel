import React from "react";
import { Book } from "react-feather";
import { revenueGeneratedSeries, revenueGenerated } from "../StatisticsData";
import StatisticsCards from "./../../@vuexy/statisticsCard/StatisticsCard";

const RevenueGenerated = () => {
  return (
    <>
      <StatisticsCards
        icon={<Book className="success" size={22} />}
        iconBg="success"
        stat="40 دوره"
        statTitle="تعداد کل دوره ها"
        options={revenueGenerated}
        series={revenueGeneratedSeries}
        type="area"
      />
    </>
  );
};

export { RevenueGenerated };
