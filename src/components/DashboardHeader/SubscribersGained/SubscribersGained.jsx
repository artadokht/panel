import React, { useState, useEffect } from "react";
import { Users } from "react-feather";
import { subscribersGained, subscribersGainedSeries } from "../StatisticsData";
import StatisticsCards from "./../../@vuexy/statisticsCard/StatisticsCard";
import { getStudents } from "../../../core/api/studentServices";

const SubscribersGained = () => {
  const [students, setStudents] = useState([]);

  useEffect(async () => {
    setStudents(await getStudents());
  }, []);
  return (
    <>
      <StatisticsCards
        icon={<Users className="primary" size={22} />}
        stat={`${students.length} نفر`}
        statTitle="تعداد کل دانشجویان"
        options={subscribersGained}
        series={subscribersGainedSeries}
        type="area"
      />
    </>
  );
};

export { SubscribersGained };
