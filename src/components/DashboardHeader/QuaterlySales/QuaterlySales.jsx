import React from "react";
import { Eye } from "react-feather";
import { quaterlySales, quaterlySalesSeries } from "../StatisticsData";
import StatisticsCards from "./../../@vuexy/statisticsCard/StatisticsCard";

const QuaterlySales = () => {
  return (
    <>
      <StatisticsCards
        icon={<Eye className="danger" size={22} />}
        iconBg="danger"
        stat="20 نفر"
        statTitle="بازدیدکننده ها"
        options={quaterlySales}
        series={quaterlySalesSeries}
        type="area"
      />
    </>
  );
};

export { QuaterlySales };
