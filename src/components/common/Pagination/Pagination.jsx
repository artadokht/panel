import React, { useState, useEffect } from "react";
import ScrollAnimation from "react-animate-on-scroll";
import styles from "./Pagination.module.css";
import {
  MDBPagination,
  MDBPageItem,
  MDBPageNav,
  MDBCol,
  MDBRow,
} from "mdbreact";

const Pagination = ({ setActive, itemCount, pageSize, active }) => {
  // const [active, setActive] = useState(activePage);

  const handleClick = (pageNumber) => {
    if (pageNumber == "next") {
      let page = active + 1;
      setActive(page);
    } else if (pageNumber == "prev") {
      let page = active - 1;
      setActive(page);
    } else {
      setActive(pageNumber);
    }
  };

  useEffect(() => {
    setActive(1);
  }, [itemCount]);

  useEffect(() => {
    setActive(active);
  }, [active]);

  const arrayMaker = () => {
    let paginationCount = Math.ceil(itemCount / pageSize);
    let array = Array.from({ length: paginationCount }, (_, index) => ++index);
    return array;
  };

  return (
    <ScrollAnimation
      animateIn="fadeInDown"
      duration={1.5}
      offset={50}
      className={styles.holder}
    >
      <MDBRow>
        <MDBCol>
          <MDBPagination className={"my-2 " + styles.ul}>
            <MDBPageItem
              disabled={active > 1 ? false : true}
              onClick={() => handleClick("prev")}
            >
              <MDBPageNav aria-label="Previous">
                <span aria-hidden="true">قبلی</span>
              </MDBPageNav>
            </MDBPageItem>
            {arrayMaker().map((item, index) => (
              <MDBPageItem
                active={item === active ? true : false}
                onClick={() => {
                  handleClick(item);
                }}
                key={index}
              >
                <MDBPageNav key={index}>
                  {item} <span className="sr-only">(current)</span>
                </MDBPageNav>
              </MDBPageItem>
            ))}
            <MDBPageItem
              disabled={
                active === Math.ceil(itemCount / pageSize) ? true : false
              }
              onClick={() => {
                console.log("run");
                handleClick("next");
              }}
            >
              <MDBPageNav aria-label="Previous">
                <span aria-hidden="true">بعدی</span>
              </MDBPageNav>
            </MDBPageItem>
          </MDBPagination>
        </MDBCol>
      </MDBRow>
    </ScrollAnimation>
  );
};

export default Pagination;
