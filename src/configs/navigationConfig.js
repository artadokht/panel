import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "dashboard",
    title: "داشبورد",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard"
  },
  // {
  //   id: "home",
  //   title: "Home",
  //   type: "item",
  //   icon: <Icon.Home size={20} />,
  //   permissions: ["admin", "editor"],
  //   navLink: "/"
  // },
  {
    id: "dropdown",
    title: "دوره ها",
    type: "collapse",
    icon: <Icon.Book size={20} />,
    // badge: "warning",
    // badgeText: "2",
    children: [
      {
        id: "analyticsDash",
        title: "کل دوره ها",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin", "editor"],
        navLink: "/test"
      },
      {
        id: "eCommerceDash",
        title: "اضافه کردن دوره",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
      {
        id: "eCommerceDash",
        title: "ویرایش دوره",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
    ]
  },
  

  {
    id: "dropdown2",
    title: "اساتید",
    type: "collapse",
    icon: <Icon.Users size={20} />,
    // badge: "warning",
    // badgeText: "2",
    children: [
      {
        id: "analyticsDash",
        title: "کل اساتید",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin", "editor"],
        navLink: "/test"
      },
      {
        id: "eCommerceDash",
        title: "اضافه کردن استاد",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
      {
        id: "eCommerceDash",
        title: "ویرایش استاد",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
    ]
  },



  {
    id: "dropdown3",
    title: "دانشجویان",
    type: "collapse",
    icon: <Icon.Users size={20} />,
    // badge: "warning",
    // badgeText: "2",
    children: [
      {
        id: "analyticsDash",
        title: "کل دانشجویان",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin", "editor"],
        navLink: "/allstudents"
      },
      {
        id: "eCommerceDash",
        title: "اضافه کردن دانشجو",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
      {
        id: "eCommerceDash",
        title: "ویرایش دانشجو",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
    ]
  },


  {
    id: "dropdown4",
    title: "ترم ها",
    type: "collapse",
    icon: <Icon.BookOpen size={20} />,
    // badge: "warning",
    // badgeText: "2",
    children: [
      {
        id: "analyticsDash",
        title: "کل ترم ها",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin", "editor"],
        navLink: "/test"
      },
      {
        id: "eCommerceDash",
        title: "اضافه کردن ترم",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
      {
        id: "eCommerceDash",
        title: "ویرایش ترم",
        type: "item",
        icon: <Icon.Circle size={12} />,
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
    ]
  },


  {
    id: "eCommerceDash",
    title: "خروج",
    type: "item",
    icon: <Icon.LogOut size={12} />,
    permissions: ["admin"],
    navLink: "/ecommerce-dashboard"
  },
]

export default navigationConfig
